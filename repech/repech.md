# Sujet Rattrapage B2


## I. Setup


L'install :

```
[arthur@localhost ~]$ sudo dnf install https://github.com/MediaBrowser/Emby.Releases/releases/download/4.7.11.0/emby-server-rpm_4.7.11.0_x86_64.rpm
``` 

Vérification :

```
[arthur@localhost /]$ sudo systemctl start emby-server
[arthur@localhost /]$ sudo systemctl status emby-server
● emby-server.service - Emby Server is a personal media server with apps on just about every device
     Loaded: loaded (/usr/lib/systemd/system/emby-server.service; enabled; vendor preset: disabled)
     Active: active (running) since Tue 2023-02-07 14:56:32 CET; 6min ago
   Main PID: 44074 (EmbyServer)
      Tasks: 13 (limit: 5905)
     Memory: 109.1M
        CPU: 5.961s
     CGroup: /system.slice/emby-server.service
             └─44074 /opt/emby-server/system/EmbyServer -programdata /var/lib/emby -ffdetect /opt/emby-server/bin/ffdet>

Feb 07 14:56:37 web emby-server[44074]: Info App: Entry point completed: Emby.Notifications.Notifications. Duration: 0.>
Feb 07 14:56:37 web emby-server[44074]: Info App: Starting entry point Emby.Server.Sync.SyncManagerEntryPoint
Feb 07 14:56:37 web emby-server[44074]: Info App: SyncRepository Initialize taking write lock
Feb 07 14:56:37 web emby-server[44074]: Info App: SyncRepository Initialize write lock taken
Feb 07 14:56:37 web emby-server[44074]: Info App: Entry point completed: Emby.Server.Sync.SyncManagerEntryPoint. Durati>
Feb 07 14:56:37 web emby-server[44074]: Info App: Starting entry point Emby.Server.Sync.SyncNotificationEntryPoint
Feb 07 14:56:37 web emby-server[44074]: Info App: Entry point completed: Emby.Server.Sync.SyncNotificationEntryPoint. D>
Feb 07 14:56:37 web emby-server[44074]: Info App: Starting entry point EmbyServer.Windows.LoopUtilEntryPoint
Feb 07 14:56:37 web emby-server[44074]: Info App: Entry point completed: EmbyServer.Windows.LoopUtilEntryPoint. Duratio>
Feb 07 14:56:37 web emby-server[44074]: Info App: All entry points have started
lines 1-20/20 (END)

[arthur@localhost /]$ sudo systemctl enable emby-server
```

Consulter les logs :

```
[arthur@localhost /]$ sudo journalctl -u emby-server
```

Les process :

```
[arthur@localhost /]$ ps | grep emby
[arthur@localhost /]$ ps aux | grep emby-server
emby       44074  0.7 17.2 3173268 169516 ?      Ssl  14:56   0:06 /opt/emby-server/system/EmbyServer -programdata /var/lib/emby -ffdetect /opt/emby-server/bin/ffdetect -ffmpeg /opt/emby-server/bin/ffmpeg -ffprobe /opt/emby-server/bin/ffprobe -restartexitcode 3 -updatepackage emby-server-rpm_{version}_x86_64.rpm
arthur     44276  0.0  0.2   6408  2152 pts/0    S+   15:09   0:00 grep --color=auto emby-server
```

Port sur lequel Emby tourne :

```
[arthur@localhost /]$ sudo ss -altnp | grep EmbyServer
LISTEN 0      512                *:8096            *:*    users:(("EmbyServer",pid=44294,fd=193))
```

L'utilisateur qui lance emby :

```
[arthur@localhost /]$ ps aux | grep emby-server
emby       44294  1.6 16.2 3172408 160164 ?      Ssl  15:11   0:06 /opt/emby-server/system/EmbyServer -programdata /var/lib/emby -ffdetect /opt/emby-server/bin/ffdetect -ffmpeg /opt/emby-server/bin/ffmpeg -ffprobe /opt/emby-server/bin/ffprobe -restartexitcode 3 -updatepackage emby-server-rpm_{version}_x86_64.rpm
arthur     44377  0.0  0.2   6408  2172 pts/0    S+   15:18   0:00 grep --color=auto emby-server
```

C'est donc l'utilisateur arthur qui lance le process

Accéder à l'interface web :

Ouvrir le port :

```
[arthur@localhost /]$ sudo firewall-cmd --permanent --add-port=8096/tcp
success
[arthur@localhost /]$ sudo firewall-cmd --reload
success
```

Configuration du répertoire pour acceuillir les média :

Créer le répertoire :

```
[arthur@localhost /]$ sudo mkdir /srv/media
```

Donner les droits au bon User :

```
[arthur@localhost media]$ sudo chown emby:emby /srv/media/
```

Donner les droits minimum :

```
[arthur@localhost media]$ sudo chmod 750 /srv/media/
```

Envoyer le fichier audio :

```
PS C:\WINDOWS\system32> scp C:\Users\arnau\Music\Musique-tp-linux.mp3 emby@10.104.1.11:/srv/media
emby@10.104.1.11's password:
Musique-tp-linux.mp3                                                                  100% 5053KB  67.6MB/s   00:00
```

Après avoir ajouté notre chemin dans les paramètres et dans la médiathèque, on peut maintenant écouter notre musique à l'acceuil.


## 2. Reverse Proxy

On installe Nginx :

```
[arthur@localhost ~]$ sudo dnf install nginx -y
[...]
Complete !
```

On démarre Nginx :

```
[arthur@localhost ~]$ sudo systemctl start nginx
[arthur@localhost ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
     Active: active (running) since Wed 2023-02-08 14:36:51 CET; 5s ago
    Process: 40371 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
    Process: 40372 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
    Process: 40373 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
   Main PID: 40374 (nginx)
      Tasks: 2 (limit: 5905)
     Memory: 1.9M
        CPU: 25ms
     CGroup: /system.slice/nginx.service
             ├─40374 "nginx: master process /usr/sbin/nginx"
             └─40375 "nginx: worker process"

Feb 08 14:36:51 proxy systemd[1]: Starting The nginx HTTP and reverse proxy server...
Feb 08 14:36:51 proxy nginx[40372]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
Feb 08 14:36:51 proxy nginx[40372]: nginx: configuration file /etc/nginx/nginx.conf test is successful
Feb 08 14:36:51 proxy systemd[1]: Started The nginx HTTP and reverse proxy server.
```

On repère le port de Nginx :

```
[arthur@localhost ~]$ sudo ss -alpnt
State  Recv-Q Send-Q  Local Address:Port   Peer Address:Port Process
LISTEN 0      511           0.0.0.0:80          0.0.0.0:*     users:(("nginx",pid=40375,fd=6),("nginx",pid=40374,fd=6))
LISTEN 0      128           0.0.0.0:22          0.0.0.0:*     users:(("sshd",pid=21221,fd=3))
LISTEN 0      511              [::]:80             [::]:*     users:(("nginx",pid=40375,fd=7),("nginx",pid=40374,fd=7))
LISTEN 0      128              [::]:22             [::]:*     users:(("sshd",pid=21221,fd=4))
```

On Ouvre le Firewall :

```
[arthur@localhost ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[arthur@localhost ~]$ sudo firewall-cmd --reload
success
```

On détermine l'utilisateur de Nginx :

```
[arthur@localhost ~]$ ps -ef | grep nginx
root       40374       1  0 14:36 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx      40375   40374  0 14:36 ?        00:00:00 nginx: worker process
arthur     40402     960  0 14:37 pts/0    00:00:00 grep --color=auto nginx
```

On vérifi que la page d'accueuil de Nginx est joignable :

```
[arthur@localhost ~]$ curl 10.104.1.14:80
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/
[...]
```

Configuration de Nginx :

```
[arthur@localhost ~]$ cat /etc/nginx/conf.d/nginx2.conf
server {
    # On indique le nom que client va saisir pour accéder au service
    # Pas d'erreur ici, c'est bien le nom de web, et pas de proxy qu'on veut ici !
    server_name web.peche.linux;

    # Port d'écoute de NGINX
    listen 80;

    location / {
        # On définit des headers HTTP pour que le proxying se passe bien
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        # On définit la cible du proxying
        proxy_pass http://10.104.1.11:8096;
    }

    # Deux sections location recommandés par la doc NextCloud
    location /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
      return 301 $scheme://$host/remote.php/dav;
    }
}
```

Mise en place de HTTPS :

```
[arthur@localhost ~]$ openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout server.key -out server.crt
Common Name (eg, your name or your server's hostname) []:web.peche.linux
[...]
```

```
[arthur@localhost ~]$ sudo mv server.crt /etc/pki/tls/certs/
[arthur@localhost ~]$ sudo mv server.key /etc/pki/tls/private/
```

On modifie la conf :

```
[arthur@localhost ~]$ sudo cat /etc/nginx/conf.d/nginx2.conf
server {
    # On indique le nom que client va saisir pour accéder au service
    # Pas d'erreur ici, c'est bien le nom de web, et pas de proxy qu'on veut ici !
    server_name web.peche.linux;


    # Port d'écoute de NGINX
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    ssl_certificate     /etc/pki/tls/certs/server.crt;
    ssl_certificate_key   /etc/pki/tls/private/server.key;

    location / {
        # On définit des headers HTTP pour que le proxying se passe bien
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        # On définit la cible du proxying
        proxy_pass http://10.104.1.11:8096;
    }

    # Deux sections location recommandés par la doc NextCloud
    location /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
      return 301 $scheme://$host/remote.php/dav;
    }
}
server {
    listen 80 default_server;
    server_name _;
    return 301 https://$host$request_uri;
}
```
Après avoir modifier le fichier host du pc :

```
10.104.1.14	emby.peche.linux
```

On peut restart nginx et normalement TADAM...
Un HTTPS sauvage est apparu

Il faut maintenant le rendre injoignable depuis depuis l'ip de emby :

```
[arthur@localhost ~]$ cat /etc/sysctl.conf | tail -n 1
net.ipv4.icmp_echo_ignore_all = 1
[arthur@localhost ~]$ sudo sysctl -p
net.ipv4.icmp_echo_ignore_all = 1
```

On teste :

```
PING 10.104.1.11 (10.104.1.11) 56(84) bytes of data.
^C
--- 10.104.1.11 ping statistics ---
4 packets transmitted, 0 received, 100% packet loss, time 3056ms
```


## 3. Backup

### A. Serveur NFS

Création des répertoires :

```
[arthur@localhost ~]$ sudo mkdir backups
[arthur@localhost ~]$ sudo mkdir backups/music
```

Installer NFS :

```
[arthur@localhost ~]$ sudo dnf install nfs-utils -y
[...]
Complete!
```

Partager le dossier avec web.peche.linux :

```
[arthur@localhost ~]$ cat /etc/exports
/backups/music    client_ip(rw,sync,no_root_squash,no_subtree_check)
```

Adaptation des règle du firewall :

```
[arthur@localhost ~]$ sudo firewall-cmd --permanent --add-service=nfs
success
[arthur@localhost ~]$ sudo firewall-cmd --permanent --add-service=mountd
success
[arthur@localhost ~]$ sudo firewall-cmd --permanent --add-service=rpc-bind
success
[arthur@localhost ~]$ sudo firewall-cmd --reload
success
```

### B. Client NFS

Création du répertoire :

```
[arthur@localhost ~]$ sudo mkdir backup
```

Installation de NFS sur le client :

```
[arthur@localhost ~]$ sudo dnf install nfs-utils -y
[...]
Complete!

[arthur@localhost ~]$ sudo mount -t nfs 10.104.1.15:backups/music/web.peche.linux/ backup/
[arthur@localhost ~]$ cat /etc/fstab | grep nfs
10.104.1.15:backups/music/web.peche.linux/ backup/ nfs defaults 0 0 
```

### C. Script de Backup

[Script backup](./backup.sh)

[Service](./backup.service)

Création du user qui gère le service de backup et le dossier :

```
[arthur@localhost ~]$ sudo useradd backup
[arthur@localhost ~]$ sudo chown backup backup/
[arthur@localhost ~]$ sudo chmod 750 backup/
[arthur@localhost ~]$ sudo systemctl daemon-reload
[arthur@localhost ~]$ sudo systemctl start backup
[arthur@localhost ~]$ sudo systemctl enable backup
Created symlink /etc/systemd/system/multi-user.target.wants/backup.service → /etc/systemd/system/backup.service.
[arthur@localhost ~]$ sudo systemctl status backup
○ backup.service - Backup service for emby config themes and data files.
     Loaded: loaded (/etc/systemd/system/backup.service; enabled; vendor preset: disabled)
     Active: inactive (dead)

Feb 08 05:12:43 web systemd[49095]: backup.service: Failed to determine user credentials: No such process
Feb 08 05:12:43 web systemd[49095]: backup.service: Failed at step USER spawning /usr/bin/bash: No such process
Feb 08 05:12:43 web systemd[1]: backup.service: Main process exited, code=exited, status=217/USER
Feb 08 05:12:43 web systemd[1]: backup.service: Failed with result 'exit-code'.
Feb 08 05:12:43 web systemd[1]: Failed to start Backup service for emby config themes and data files..
Feb 08 05:17:33 web systemd[1]: Starting Backup service for emby config themes and data files....
Feb 08 05:17:33 web bash[49164]: backup.sh: line 7: /usr/bin/tar: No such file or directory
Feb 08 05:17:33 web bash[49162]: La création du fichier de backup a échoué, veuillez vérifier les erreurs.
Feb 08 05:17:33 web systemd[1]: backup.service: Deactivated successfully.
Feb 08 05:17:33 web systemd[1]: Finished Backup service for emby config themes and data files..
```

[Timer](./backup.timer)