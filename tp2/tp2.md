# TP2 : Gestion de service


# I. Un premier serveur web

## 1. Installation

🌞 **Installer le serveur Apache**
```
[arthur@web ~]$ sudo dnf -y install httpd
Complete!
```

🌞 **Démarrer le service Apache**

```
[arthur@web ~]$ sudo ss -ltpn
State           Recv-Q          Send-Q                   Local Address:Port                   Peer Address:Port         Process                                                                                                              
LISTEN          0               4096                           0.0.0.0:111                         0.0.0.0:*             users:(("rpcbind",pid=687,fd=4),("systemd",pid=1,fd=57))                                                            
LISTEN          0               128                            0.0.0.0:22                          0.0.0.0:*             users:(("sshd",pid=9793,fd=3))                                                                                      
LISTEN          0               4096                              [::]:111                            [::]:*             users:(("rpcbind",pid=687,fd=6),("systemd",pid=1,fd=59))                                                            
LISTEN          0               511                                  *:80                                *:*             users:(("httpd",pid=51931,fd=4),("httpd",pid=51930,fd=4),("httpd",pid=51929,fd=4),("httpd",pid=51927,fd=4))         
LISTEN          0               128                               [::]:22                             [::]:*             users:(("sshd",pid=9793,fd=4))                                                                                         
```

🌞 **TEST**
```
[arthur@web ~]$ systemctl status httpd
● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
     Active: active (running) since Tue 2022-11-15 17:52:37 UTC; 5min ago
       Docs: man:httpd.service(8)
   Main PID: 51927 (httpd)
     Status: "Total requests: 0; Idle/Busy workers 100/0;Requests/sec: 0; Bytes served/sec:   0 B/sec"
      Tasks: 213 (limit: 4599)
     Memory: 23.8M
        CPU: 219ms
     CGroup: /system.slice/httpd.service
             ├─51927 /usr/sbin/httpd -DFOREGROUND
             ├─51928 /usr/sbin/httpd -DFOREGROUND
             ├─51929 /usr/sbin/httpd -DFOREGROUND
             ├─51930 /usr/sbin/httpd -DFOREGROUND
             └─51931 /usr/sbin/httpd -DFOREGROUND
```

## 2. Avancer vers la maîtrise du service

🌞 **Le service Apache...**

```
[arthur@web ~]$ systemctl cat httpd
# See httpd.service(8) for more information on using the httpd service.

# Modifying this file in-place is not recommended, because changes
# will be overwritten during package upgrades.  To customize the
# behaviour, run "systemctl edit httpd" to create an override unit.

# For example, to pass additional options (such as -D definitions) to
# the httpd binary at startup, create an override unit (as is done by
# systemctl edit) and enter the following:

#	[Service]
#	Environment=OPTIONS=-DMY_DEFINE

[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true
OOMarthuricy=continue

[Install]
WantedBy=multi-user.target
```

🌞 **Déterminer sous quel utilisateur tourne le processus Apache**

```
[arthur@web ~]$ cat /etc/httpd/conf/httpd.conf 
[...]
User apache
Group apache
[...]
``` 
```
[arthur@web ~]$ ps -ef | grep 51927
root       51927       1  0 17:52 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     51928   51927  0 17:52 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     51929   51927  0 17:52 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     51930   51927  0 17:52 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     51931   51927  0 17:52 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
```
```
[arthur@web ~]$ ls -la /usr/share/testpage/           
total 12
drwxr-xr-x.  2 root root   24 Nov 15 17:51 .
drwxr-xr-x. 96 root root 4096 Nov 15 17:51 ..
-rw-r--r--.  1 root root 7620 Jul  6 02:37 index.html
```
L'utilisateur mentionné n'a pas accès au fichier mais le processus parent y accès en étant root.

🌞 **Changer l'utilisateur utilisé par Apache**

```
[arthur@web ~]$ ps -ef | grep 52732
root       52732       1  0 18:34 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
newapac+   52733   52732  0 18:34 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
newapac+   52734   52732  0 18:34 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
newapac+   52735   52732  0 18:34 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
newapac+   52736   52732  0 18:34 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
```

🌞 **Faites en sorte que Apache tourne sur un autre port**

```
[arthur@web ~]$ sudo ss -ltpn
State        Recv-Q        Send-Q               Local Address:Port               Peer Address:Port       Process                                                                                                  
[...]                                              
LISTEN       0             511                              *:8080                          *:*           users:(("httpd",pid=53024,fd=4),("httpd",pid=53023,fd=4),("httpd",pid=53022,fd=4),("httpd",pid=53018,fd=4))
[...] 
```
J'accède à la page avec `curl localhost:8080` et j'accède à la page avec mon navigateur.

📁 **Fichier [/etc/httpd/conf/httpd.conf](files/httpd.conf)**

# II. Une stack web plus avancée

## 2. Setup

### A. Base de données

🌞 **Install de MariaDB sur `db.tp2.linux`**

```
[arthur@db ~]$ sudo dnf -y install mariadb-server
[arthur@db ~]$ sudo systemctl enable mariadb
[arthur@db ~]$ sudo systemctl start mariadb
[arthur@db ~]$ sudo mysql_secure_installation
```

🌞 **Préparation de la base pour NextCloud**

```
[arthur@db ~]$ sudo mysql -u root -p
Enter password: 
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 9
Server version: 10.5.16-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.3.0.4' IDENTIFIED BY "pewpewpew";
Query OK, 0 rows affected (0.002 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10,';
ERROR 1133 (28000): Can't find any matching row in the user table
MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.3.0.4';
Query OK, 0 rows affected (0.002 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)
```

🌞 **Exploration de la base de données**

```
mysql> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.00 sec)

mysql> USE nextcloud;
Database changed
mysql> SHOW TABLES;
Empty set (0.00 sec)
```
🌞 **Trouver une commande SQL qui permet de lister tous les utilisateurs de la base de données**

```
MariaDB [nextcloud]> SELECT user FROM mysql.user;
+-------------+
| User        |
+-------------+
| nextcloud   |
| mariadb.sys |
| mysql       |
| root        |
+-------------+
4 rows in set (0.001 sec)
```

### B. Serveur Web et NextCloud

🌞 **Install de PHP**

```
[arthur@web ~]$ sudo dnf config-manager --set-enabled crb
[arthur@web ~]$ sudo dnf install dnf-utils http://rpms.remirepo.net/enterprise/remi-release-9.rpm -y
[arthur@web ~]$ dnf module list php
[arthur@web ~]$ sudo dnf module enable php:remi-8.1 -y
[arthur@web ~]$ sudo dnf install -y php81-php
Complete!
```

🌞 **Install de tous les modules PHP nécessaires pour NextCloud**

```
[arthur@web ~]$ sudo dnf install -y libxml2 openssl php81-php php81-php-ctype php81-php-curl php81-php-gd php81-php-iconv php81-php-json php81-php-libxml php81-php-mbstring php81-php-openssl php81-php-posix php81-php-session php81-php-xml php81-php-zip php81-php-zlib php81-php-pdo php81-php-mysqlnd php81-php-intl php81-php-bcmath php81-php-gmp
Complete!
```

🌞 **Récupérer NextCloud**

```
[arthur@web tp2_nextcloud]$ ls -al
total 140
drwxr-xr-x. 14 root root  4096 Nov 15 21:32 .
drwxr-xr-x.  5 root root    54 Nov 15 21:26 ..
-rw-r--r--.  1 root root  3253 Oct  6 12:42 .htaccess
-rw-r--r--.  1 root root   101 Oct  6 12:42 .user.ini
drwxr-xr-x. 47 root root  4096 Oct  6 12:47 3rdparty
-rw-r--r--.  1 root root 19327 Oct  6 12:42 AUTHORS
-rw-r--r--.  1 root root 34520 Oct  6 12:42 COPYING
drwxr-xr-x. 50 root root  4096 Oct  6 12:44 apps
drwxr-xr-x.  2 root root    67 Oct  6 12:47 config
-rw-r--r--.  1 root root  4095 Oct  6 12:42 console.php
drwxr-xr-x. 23 root root  4096 Oct  6 12:47 core
-rw-r--r--.  1 root root  6317 Oct  6 12:42 cron.php
drwxr-xr-x.  2 root root  8192 Oct  6 12:42 dist
-rw-r--r--.  1 root root   156 Oct  6 12:42 index.html
-rw-r--r--.  1 root root  3456 Oct  6 12:42 index.php
drwxr-xr-x.  6 root root   125 Oct  6 12:42 lib
-rw-r--r--.  1 root root   283 Oct  6 12:42 occ
drwxr-xr-x.  2 root root    23 Oct  6 12:42 ocm-provider
drwxr-xr-x.  2 root root    55 Oct  6 12:42 ocs
drwxr-xr-x.  2 root root    23 Oct  6 12:42 ocs-provider
-rw-r--r--.  1 root root  3139 Oct  6 12:42 public.php
-rw-r--r--.  1 root root  5426 Oct  6 12:42 remote.php
drwxr-xr-x.  4 root root   133 Oct  6 12:42 resources
-rw-r--r--.  1 root root    26 Oct  6 12:42 robots.txt
-rw-r--r--.  1 root root  2452 Oct  6 12:42 status.php
drwxr-xr-x.  3 root root    35 Oct  6 12:42 themes
drwxr-xr-x.  2 root root    43 Oct  6 12:44 updater
-rw-r--r--.  1 root root   387 Oct  6 12:47 version.php
```

🌞 **Adapter la configuration d'Apache**
```
[arthur@web tp2_nextcloud]$ cat /etc/httpd/conf.d/nextcloud.conf
<VirtualHost *:80>
  # on indique le chemin de notre webroot
  DocumentRoot /var/www/tp2_nextcloud/
  # on précise le nom que saisissent les clients pour accéder au service
  ServerName  web.tp2.linux

  # on définit des règles d'accès sur notre webroot
  <Directory /var/www/tp2_nextcloud/> 
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews
    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```
🌞 **Redémarrer le service Apache** pour qu'il prenne en compte le nouveau fichier de conf

### C. Finaliser l'installation de NextCloud

➜ **Sur votre PC**

```
##
# Host Database
#
# localhost is used to configure the loopback interface
# when the system is booting.  Do not change this entry.
##
127.0.0.1	localhost
255.255.255.255	broadcasthost
::1             localhost
10.102.1.11   web.tp2.linux
```

🌞 **Exploration de la base de données**

```
MariaDB [nextcloud]> SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE';
+----------+
| COUNT(*) |
+----------+
|      205 |
+----------+
1 row in set (0.004 sec)
```
