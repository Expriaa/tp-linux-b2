# TP5 - Streaming musique avec Koel


# I- Installation de Koel 


🖥️ VM ``koel.tp5.linux`` : 10.105.1.11

    ```
    [arthur@koel]$ mkdir projet-koel
    [arthur@koel]$ cd projet-koel/
    [arthur@koel projet-koel]$ sudo dnf install git
    [arthur@koel projet-koel]$ git clone https://github.com/koel/koel.git .
    [arthur@koel projet-koel]$ git checkout latest
    ```

- Installation de PHP :

    - Prérequis :

    ```
    [arthur@koel projet-koel]$ sudo dnf update -y
    [arthur@koel projet-koel]$ sudo dnf config-manager --set-enabled crb
    [arthur@koel projet-koel]$ sudo dnf install epel-release
    [arthur@koel projet-koel]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-9.rpm

    ```

    - Installation PHP :

    ```
    [arthur@koel projet-koel]$ sudo dnf module enable php:remi-8.0
    [arthur@koel projet-koel]$ sudo dnf install php php-zip php-intl php-mysqlnd php-dom php-simplexml php-xml php-xmlreader php-curl php-exif php-ftp php-gd php-json php-mbstring php-posix
    ```

- Installation Composer :
```
[arthur@koel projet-koel]$ sudo dnf install wget -y
```

```
[arthur@koel projet-koel]$ php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"

[arthur@koel projet-koel]$ php -r "if (hash_file('sha384', 'composer-setup.php') === '$(wget -qO - https://composer.github.io/installer.sig)') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"

[arthur@koel projet-koel]$ php composer-setup.php
[arthur@koel projet-koel]$ sudo mv composer.phar /usr/local/bin/composer
[arthur@koel projet-koel]$ composer install
```

Unzip de Composer :
```
[arthur@koel projet-koel]$ mkdir dir-composer
[arthur@koel projet-koel]$ cd dir-composer
[arthur@koel dir-composer]$ gunzip composer
[arthur@koel dir-composer]$ mv composer composer.gz
[arthur@koel dir-composer]$ gunzip composer.gz
[arthur@koel dir-composer]$ sudo mv composer /usr/local/bin/
[arthur@koel projet-koel]$ cd ..
[arthur@koel projet-koel]$ rm -rf dir-composer
```

- Installation de Mariadb :

    ```
    [arthur@koel ~]$ sudo dnf install mariadb-server -y
    [arthur@koel ~]$ sudo systemctl enable mariadb
    [arthur@koel ~]$ sudo systemctl start mariadb
    ```

    - Ouverture du port :
    ```
    [arthur@koel ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
    [arthur@koel ~]$ sudo firewall-cmd --reload
    ```

    - Création de la DB :
    ```
    [arthur@koel projet-koel]$ sudo mysql -u root -p

    MariaDB [(none)]> CREATE USER 'arthur'@'10.105.1.11' IDENTIFIED BY '123';
    Query OK, 0 rows affected (0.004 sec)

    MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS koel CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
    Query OK, 0 rows affected (0.002 sec)

    MariaDB [(none)]> GRANT ALL PRIVILEGES ON koel.* TO 'arthur'@'10.105.1.11';

    MariaDB [(none)]> FLUSH PRIVILEGES;
    Query OK, 0 rows affected (0.001 sec)
    ```

- Installation de yarn :
    ```
    [arthur@koel projet-koel]$ sudo dnf install npm
    [arthur@koel projet-koel]$ sudo npm install --global yarn
    ```

    - Config :
    ```
    [arthur@koel projet-koel]$ php artisan koel:init
    ************************************
    *     KOEL INSTALLATION WIZARD     *
    ************************************

    Clearing caches ........................................................................................... 4ms DONE
    INFO  .env file exists -- skipping.

    Retrieving app key ........................................................................................ 1ms DONE

    INFO  Using app key: base64:VHyd72ira...

    WARN  Cannot connect to the database. Let's set it up.

    Your DB driver of choice [MySQL/MariaDB]:
    [mysql     ] MySQL/MariaDB
    [pgsql     ] PostgreSQL
    [sqlsrv    ] SQL Server
    [sqlite-e2e] SQLite
    > mysql

    DB host:
    > 10.105.1.11

    DB port (leave empty for default):
    >

    DB name:
    > koel

    DB user:
    > arthur

    DB password:
    > 123

    Migrating database ...................................................................................... 731ms DONE
    Creating default admin account ........................................................................... 72ms DONE
    Seeding data ............................................................................................. 47ms DONE

    The absolute path to your media directory. If this is skipped (left blank) now, you can set it later via the web interface.

    Media path []:
    >

    yarn install v1.22.19
    [1/4] Resolving packages...
    [2/4] Fetching packages...

    [...]

    [OK] All done!
    ```

    Modification de localhost par 10.105.1.11 :

    ```
    [arthur@koel projet-koel]$ sudo nano .env

    APP_URL=http://10.105.1.11:8000
    ```

    Ouverture du port :
    ```
    [arthur@koel projet-koel]$ sudo firewall-cmd --add-port=8000/tcp
    [arthur@koel projet-koel]$ sudo firewall-cmd --add-port=8000/tcp permanent
    [arthur@koel projet-koel]$ sudo firewall-cmd --add-port=8000/tcp --permanent
    ```


    - Let's start it :
    ```
    [arthur@koel projet-koel]$ php artisan serve --host  10.105.1.11

    INFO  Server running on [http://10.105.1.11:8000].

    Press Ctrl+C to stop the server
    ```


# II - Apache

- Installation :

```
[arthur@koel projet-koel]$ sudo dnf install httpd -y
Complete!
```

- Démarrage du service Apache :
```
[arthur@koel projet-koel]$ sudo systemctl start httpd
[arthur@koel projet-koel]$ sudo systemctl enable httpd
```

- Ouverture du port :
```
[arthur@koel projet-koel]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[arthur@koel projet-koel]$ sudo firewall-cmd --reload
success
```

- Création du fichier de conf :
```
[arthur@koel projet-koel]$ sudo nano /etc/httpd/conf.d/koel.conf

<VirtualHost *:80>
  DocumentRoot /var/www/koel/public/
  ServerName  koel.tp5.linux
  <Directory /var/www/koel/public/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews
    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```

- Déplacement du projet :
```
[arthur@koel ~]$ mv projet-koel/ /var/www/
[arthur@koel ~]$ cd /var/www/
[arthur@koel ~]$ mv projet-koel koel
[arthur@koel ~]$ sudo chown apache:apache -R koel/
```

- Le serveur Apache est désormais joignable sur http://10.105.1.11:80 :
```
[arthur@koel ~]$ sudo systemctl restart httpd
```

# III - Reverse Proxy

🖥️ VM ``proxy.tp5.linux`` : 10.105.1.12


- Installation de Nginx :
```
[arthur@proxy ~]$ sudo dnf install nginx -y
[arthur@proxy ~]$ sudo systemctl start nginx
[arthur@proxy ~]$ sudo systemctl enable nginx
```

- Autorisation du port 80 pour Nginx :
```
[arthur@proxy ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
[arthur@proxy ~]$ sudo firewall-cmd --reload
```

- Config de Nginx :
```
[arthur@proxy ~]$ sudo nano /etc/nginx/conf.d/proxy.conf

server {
    server_name koel.tp5.linux;
    listen 80;

    location / {
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        proxy_pass http://10.105.1.11:80;
    }

    location /.well-known/carddav {
    return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
    return 301 $scheme://$host/remote.php/dav;
    }
}

[arthur@proxy ~]$ sudo systemctl restart nginx
```

- Il faut modifier le fichier hosts notre machine :
```
10.105.1.12	koel.tp5.linux
```

On peut maintenant y accéder comme ceci :D : http://koel.tp5.linux


- HTTPS :

On génération des clés :

```
[arthur@proxy ~]$ openssl req -new -newkey rsa:2048
[arthur@proxy ~]$ sudo mv domain.crt /etc/pki/tls/certs/
[arthur@proxy ~]$ sudo mv domain.key /etc/pki/tls/private/
```

On modifie la conf de nginx :
```
[arthur@proxy ~]$ sudo nano /etc/nginx/conf.d/proxy.conf

server {
    server_name koel.tp5.linux;

    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    ssl_certificate     /etc/pki/tls/certs/domain.crt;
    ssl_certificate_key /etc/pki/tls/private/domain.key;

    location / {
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        proxy_pass http://10.105.1.11:80;
    }

    location /.well-known/carddav {
    return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
    return 301 $scheme://$host/remote.php/dav;
    }
}
server {
    listen 80 default_server;
    server_name _;
    return 301 https://$host$request_uri;
}
```

On ouvre le port 443 :

```
[arthur@proxy ~]$ sudo firewall-cmd --add-port=443/tcp --permanent
[arthur@proxy ~]$ sudo firewall-cmd --reload
```


# IV - Réplication


🖥️ VM ``replication.tp5.linux`` : 10.105.1.13

- Installation de mariadb :
    ```
    [arthur@replication ~]$ sudo dnf install mariadb-server
    [arthur@replication ~]$ sudo systemctl start mariadb
    [arthur@replication ~]$ sudo systemctl enable mariadb
    [arthur@replication ~]$ sudo mysql_secure_installation
    Thanks for using MariaDB!
    ```

- Configuration de la db :
    ```
    [arthur@koel ~]$ sudo nano /etc/my.cnf

    # This group is read both both by the client and the server
    # use it for options that affect everything
    #
    [client-server]

    #
    # include all files from the config directory
    #
    !includedir /etc/my.cnf.d
    [mysqld]
    bind-address=10.105.1.11
    server-id=1
    log_bin=mysql-bin
    binlog-format=ROW

    [arthur@koel ~]$ sudo systemctl restart mariadb
    ```

- Création d'un utilisateur :
    ```
    [arthur@koel ~]$ sudo mysql -u root -p

    MariaDB [(none)]> CREATE USER 'replication'@'%' IDENTIFIED BY '123';
    Query OK, 0 rows affected (0.091 sec)

    MariaDB [(none)]> GRANT REPLICATION SLAVE ON *.* TO 'replication'@'%';
    Query OK, 0 rows affected (0.022 sec)

    MariaDB [(none)]> FLUSH PRIVILEGES;
    Query OK, 0 rows affected (0.005 sec)

    MariaDB [(none)]> STOP SLAVE;
    Query OK, 0 rows affected, 1 warning (0.003 sec)

    MariaDB [(none)]> SHOW MASTER STATUS;
    +------------------+----------+--------------+------------------+
    | File             | Position | Binlog_Do_DB | Binlog_Ignore_DB |
    +------------------+----------+--------------+------------------+
    | mysql-bin.000002 |      792 |              |                  |
    +------------------+----------+--------------+------------------+
    1 row in set (0.002 sec)

    MariaDB [(none)]> exit
    Bye
    ```

    ```
    [arthur@koel ~]$ sudo mysqldump -u root koel > arthur.sql
    [arthur@koel ~]$ scp arthur.sql arthur@10.105.1.13:~/
    ```


- Configuration de la réplication :
    ```
    [arthur@replication ~]$ sudo nano /etc/my.cnf

    # This group is read both both by the client and the server
    # use it for options that affect everything
    #
    [client-server]

    #
    # include all files from the config directory
    #
    !includedir /etc/my.cnf.d
    [mysqld]
    bind-address=10.105.1.13
    server-id=2
    binlog-format=ROW

    [arthur@replication ~]$ sudo systemctl restart mariadb
    ```


- Création de la DB :
    ```
    [arthur@replication ~]$ sudo mysql -u root -p

    MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS koel CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
    Query OK, 1 row affected (0.003 sec)

    MariaDB [(none)]> show databases;
    +--------------------+
    | Database           |
    +--------------------+
    | information_schema |
    | mysql              |
    | performance_schema |
    | koel        |
    +--------------------+
    4 rows in set (0.002 sec)

    [arthur@replication ~]$ sudo mysql -u root -p koel<arthur.sql
    ```

- Activation de la réplication :

    ```
    [arthur@replication ~]$ sudo mysql -u root -p

    MariaDB [(none)]> STOP SLAVE;
    Query OK, 0 rows affected, 1 warning (0.001 sec)

    MariaDB [(none)]> CHANGE MASTER TO MASTER_HOST = '10.105.1.11', MASTER_USER = 'replication', MASTER_PASSWORD = '123',
    MASTER_LOG_FILE = 'mysql-bin.000002', MASTER_LOG_POS = 792;
    Query OK, 0 rows affected (0.008 sec)

    MariaDB [(none)]> start slave;
    Query OK, 0 rows affected (0.004 sec)

    MariaDB [(none)]> EXIT
    ```


- Test :
    ```
    [arthur@koel ~]$ sudo mysql -u root -p

    MariaDB [(none)]> create database koeldb;
    Query OK, 1 row affected (0.002 sec)

    MariaDB [(none)]> show databases;
    +--------------------+
    | Database           |
    +--------------------+
    | information_schema |
    | mysql              |
    | performance_schema |
    | koel               |
    | koeldb             |
    +--------------------+
    5 rows in set (0.004 sec)
    ```
    ```
    [arthur@replication ~]$ sudo mysql -u root -p

    MariaDB [(none)]> show databases;
    +--------------------+
    | Database           |
    +--------------------+
    | information_schema |
    | mysql              |
    | performance_schema |
    | koel               |
    | koeldb             |
    +--------------------+
    5 rows in set (0.003 sec)
    ```

# V - Monitoring

- Installation :
    ```
    [arthur@koel ~]$ sudo dnf install epel-release -y
    Complete!

    [arthur@koel ~]$ sudo wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp/netdata-kickstart.sh
    Successfully installed the Netdata Agent.

    [arthur@koel ~]$ sudo systemctl start netdata
    [arthur@koel ~]$ sudo systemctl enable netdata
    ```

- Ouverture du port :
    ```
    [arthur@koel ~]$ sudo ss -alnpt
    LISTEN         0               4096                           0.0.0.0:19999                        0.0.0.0:*
    users:(("netdata",pid=4096,fd=49))

    [arthur@koel ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
    [arthur@koel ~]$ sudo firewall-cmd --reload
    ```


- Let's test it :
Pour tester : http://10.105.1.11:19999


- Notification sur discord :
    ```
    [arthur@koel ~]$ sudo nano /etc/netdata/health_alarm_notify.conf

    # sending discord notifications

    # note: multiple recipients can be given like this:
    #                  "CHANNEL1 CHANNEL2 ..."

    # enable/disable sending discord notifications
    SEND_DISCORD="YES"

    # Create a webhook by following the official documentation -
    # https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
    DISCORD_WEBHOOK_URL="https://discordapp.com/api/webhooks/1044058582234181714/sGCbBWmAugHk3d98p9OM_xlwktNtLfTX3Lf_O-1Zmm>

    # if a role's recipients are not configured, a notification will be send to
    # this discord channel (empty = do not send a notification for unconfigured
    # roles):
    DEFAULT_RECIPIENT_DISCORD="général"
    ```
    ```
    [arthur@koel ~]$ sudo nano /etc/netdata/health.d/cpu_usage.conf
    alarm: cpu_usage
    on: system.cpu
    lookup: average -3s percentage foreach user, system
    units: %
    every: 10s
    warn: $this > 50
    crit: $this > 80
    info: CPU utilization of users on the system itself.
    ```
    ```
    [arthur@koel ~]$ sudo systemctl restart netdata
    [arthur@koel ~]$ sudo dnf install stress-ng
    [arthur@koel ~]$ stress-ng --vm 2 --vm-bytes 1G --timeout 60s
    ```