# Module 1 : Reverse Proxy

# I. Intro

# II. Setup

🖥️ **VM `proxy.tp3.linux`**

**N'oubliez pas de dérouler la [📝**checklist**📝](#checklist).**

➜ **On utilisera NGINX comme reverse proxy**

```
[arthur@proxy ~]$ systemctl is-active nginx && systemctl is-enabled nginx
active
enabled

[arthur@proxy ~]$ sudo ss -alntp
State    Recv-Q   Send-Q     Local Address:Port       Peer Address:Port   Process
LISTEN   0        511              0.0.0.0:80              0.0.0.0:*       users:(("nginx",pid=1779,fd=6),("nginx",pid=1778,fd=6))

[arthur@proxy ~]$ ps -ef | grep nginx
nginx       1779    1778  0 15:35 ?        00:00:00 nginx: worker process
```

➜ **Configurer NGINX**

```
[arthur@proxy ~]$ cat /etc/nginx/default.d/proxy.conf
server {
    # On indique le nom que client va saisir pour accéder au service
    # Pas d'erreur ici, c'est bien le nom de web, et pas de proxy qu'on veut ici !
    server_name web.tp2.linux;

    # Port d'écoute de NGINX
    listen 80;

    location / {
        # On définit des headers HTTP pour que le proxying se passe bien
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        # On définit la cible du proxying
        proxy_pass http://10.102.1.11:80;
    }

    # Deux sections location recommandés par la doc NextCloud
    location /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
      return 301 $scheme://$host/remote.php/dav;
    }
}
```

```
[arthur@web ~]$ sudo cat /var/www/tp2_nextcloud/config/config.php
<?php
$CONFIG = array (
  'instanceid' => 'ocx491prex4b',
  'passwordsalt' => '/nWR6YnkfJBp97X+E+Ckr2XSNI2dxG',
  'secret' => 'awTDjfZhZMsmCWC7cvPLS8dHGc9A0PBWSSV4+swIgn9ati8U',
  'trusted_domains' =>
  array (
    0 => 'web.tp2.linux',
    1 => '10.102.1.13',
  ),
  'datadirectory' => '/var/www/tp2_nextcloud/data',
  'dbtype' => 'mysql',
  'version' => '25.0.0.15',
  'overwrite.cli.url' => 'http://web.tp2.linux',
  'dbname' => 'nextcloud',
  'dbhost' => '10.102.1.12:3306',
  'dbport' => '',
  'dbtableprefix' => 'oc_',
  'mysql.utf8mb4' => true,
  'dbuser' => 'nextcloud',
  'dbpassword' => 'azerty',
  'installed' => true,
);
```

➜ **Modifier votre fichier `hosts` de VOTRE PC**

```
PS C:\WINDOWS\system32\drivers\etc> cat .\hosts
# Copyright (c) 1993-2009 Microsoft Corp.
[...]
# localhost name resolution is handled within DNS itself.
#       127.0.0.1       localhost
#       ::1             localhost
10.102.1.13 web.tp2.linux
```

✨ **Bonus** : rendre le serveur `web.tp2.linux` injoignable sauf depuis l'IP du reverse proxy. En effet, les clients ne doivent pas joindre en direct le serveur web : notre reverse proxy est là pour servir de serveur frontal.

# III. HTTPS

  ```
  [arthur@proxy ~] openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout server.key -out server.crt

  [arthur@proxy ~]$ ls /etc/pki/tls/private/
  web.tp2.linux.key

  [arthur@proxy ~]$ ls /etc/pki/tls/certs/
  ca-bundle.crt  ca-bundle.trust.crt  web.tp2.linux.crt
  ```

  ```
  [arthur@proxy ~]$ sudo cat /etc/nginx/conf.d/proxy.conf
  server {
    # On indique le nom que client va saisir pour accéder au service
    # Pas d'erreur ici, c'est bien le nom de web, et pas de proxy qu'on veut ici !
    server_name web.tp2.linux;

    # Port d'écoute de NGINX
    listen 443 ssl;

    ssl_certificate "/etc/pki/tls/certs/web.tp2.linux.crt";
    ssl_certificate_key "/etc/pki/tls/private/web.tp2.linux.key";
    [...]
  ```