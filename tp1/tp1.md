# TP1 : (re)Familiaration avec un système GNU/Linux


## 0. Préparation de la machine

🌞 **Setup de deux machines Rocky Linux configurées de façon basique.**

- **un accès internet (via la carte NAT)**
```
[arthur@node1 ~]$ ping 1.1.1.1 -c 1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=128 time=25.1 ms

--- 1.1.1.1 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 25.113/25.113/25.113/0.000 ms
```

```
[arthur@node2 ~]$ ping 1.1.1.1 -c 1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=128 time=26.1 ms

--- 1.1.1.1 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 26.119/26.119/26.119/0.000 ms
```

- **un accès à un réseau local** (les deux machines peuvent se `ping`) (via la carte Host-Only)

```
[arthur@node1 ~]$ ping 10.101.1.11 -c 1
PING 10.101.1.11 (10.101.1.11) 56(84) bytes of data.
64 bytes from 10.101.1.11: icmp_seq=1 ttl=64 time=3.45 ms

--- 10.101.1.11 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 3.446/3.446/3.446/0.000 ms
```

```
[arthur@node2 ~]$ ping 10.101.1.12 -c 1
PING 10.101.1.12 (10.101.1.12) 56(84) bytes of data.
64 bytes from 10.101.1.12: icmp_seq=1 ttl=64 time=1.17 ms

--- 10.101.1.12 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 1.169/1.169/1.169/0.000 ms
```

- **les machines doivent avoir un nom**
```
[arthur@node1 ~]$ hostname
node1.tp1.b2
```

```
[arthur@node2 ~]$ hostname
node2.tp1.b2
```

- **utiliser `1.1.1.1` comme serveur DNS**
```
[arthur@node1 ~]$ dig ynov.com
[...]
;; ANSWER SECTION:
ynov.com.		300	IN	A	172.67.74.226
ynov.com.		300	IN	A	104.26.10.233
ynov.com.		300	IN	A	104.26.11.233
[...]
;; SERVER: 1.1.1.1#53(1.1.1.1)
[...]
```
```
[arthur@node2 ~]$ dig ynov.com
[...]
;; ANSWER SECTION:
ynov.com.		300	IN	A	172.67.74.226
ynov.com.		300	IN	A	104.26.10.233
ynov.com.		300	IN	A	104.26.11.233
[...]
;; SERVER: 1.1.1.1#53(1.1.1.1)
[...]
```

- **les machines doivent pouvoir se joindre par leurs noms respectifs**
```
[arthur@node1 ~]$ ping node2.tp1.b2 -c 1
PING node2.tp1.b2 (10.101.1.11) 56(84) bytes of data.
64 bytes from node2.tp1.b2 (10.101.1.11): icmp_seq=1 ttl=64 time=1.74 ms

--- node2.tp1.b2 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 1.742/1.742/1.742/0.000 ms
```

```
[arthur@node2 ~]$ ping node1.tp1.b2 -c 1
PING node1.tp1.b2 (10.101.1.12) 56(84) bytes of data.
64 bytes from node1.tp1.b2 (10.101.1.12): icmp_seq=1 ttl=64 time=6.35 ms

--- node1.tp1.b2 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 6.350/6.350/6.350/0.000 ms
```

- **le pare-feu est configuré pour bloquer toutes les connexions exceptées celles qui sont nécessaires**

```
[arthur@node1 ~]$ sudo firewall-cmd --remove-service=cockpit --permanent
success
[arthur@node1 ~]$ sudo firewall-cmd --remove-service=dhcpv6-client --permanent
success
[arthur@node1 ~]$ sudo firewall-cmd --reload
success
[arthur@node1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s5
  sources: 
  services: ssh
  ports: 
  protocols: 
  forward: yes
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 
```

```
[arthur@node2 ~]$ sudo firewall-cmd --remove-service=cockpit --permanent
success
[arthur@node2 ~]$ sudo firewall-cmd --remove-service=dhcpv6-client --permanent
success
[arthur@node2 ~]$ sudo firewall-cmd --reload
success
[arthur@node2 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s5
  sources: 
  services: ssh
  ports: 
  protocols: 
  forward: yes
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 
```

## I. Utilisateurs

### 1. Création et configuration

🌞 **Ajouter un utilisateur à la machine**
```
[arthur@node1 ~]$ sudo cat /etc/passwd
[...]
superuser:x:1001:1001::/home/superuser:/bin/bash
```

```
[arthur@node2 ~]$ sudo cat /etc/passwd
[...]
superuser:x:1001:1001::/home/superuser:/bin/bash
```

🌞 **Créer un nouveau groupe `admins`**
```
[arthur@node1 ~]$ cat /etc/group
[...]
admins:x:1002:
```

```
[arthur@node2 ~]$ cat /etc/group
[...]
admins:x:1002:
```

🌞 **Ajouter votre utilisateur à ce groupe `admins`**

```
[superuser@node1 ~]$ sudo -l

We trust you have received the usual lecture from the local System
Administrator. It usually boils down to these three things:

    #1) Respect the privacy of others.
    #2) Think before you type.
    #3) With great power comes great responsibility.

[sudo] password for superuser: 
[...]
User superuser may run the following commands on node1:
    (ALL) ALL
```

```
[superuser@node2 ~]$ sudo -l

We trust you have received the usual lecture from the local System
Administrator. It usually boils down to these three things:

    #1) Respect the privacy of others.
    #2) Think before you type.
    #3) With great power comes great responsibility.

[sudo] password for superuser: 
[...]
User superuser may run the following commands on node2:
    (ALL) ALL
```

### 2. SSH


🌞 **Pour cela...**

```
ssh-copy-id -i .ssh/id_rsa.pub arthur@10.101.1.12
ssh-copy-id -i .ssh/id_rsa.pub arthur@10.101.1.11 
```

🌞 **Assurez vous que la connexion SSH est fonctionnelle**
```
arthur@arthu@DESKTOP-LBR4JUU MINGW64 ~ $ ssh arthur@10.101.1.12   
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Mon Nov 14 16:26:50 2022 from 10.211.55.2
[arthur@node1 ~]$ 
```

```
arthur@arthu@DESKTOP-LBR4JUU MINGW64 ~ $ssh arthur@10.101.1.11
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Mon Nov 14 16:27:30 2022 from 10.211.55.2
[arthur@node2 ~]$ 
```

## II. Partitionnement

### 2. Partitionnement


🌞 **Utilisez LVM** 

```
[arthur@node1 ~]$ lsblk
NAME              MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
sda                 8:0    0   64G  0 disk 
├─sda1              8:1    0  600M  0 part /boot/efi
├─sda2              8:2    0    1G  0 part /boot
└─sda3              8:3    0 62.4G  0 part 
  ├─rl_rocky-root 253:0    0 39.9G  0 lvm  /
  ├─rl_rocky-swap 253:1    0    3G  0 lvm  [SWAP]
  └─rl_rocky-home 253:2    0 19.5G  0 lvm  /home
sdb                 8:16   0    3G  0 disk 
sdc                 8:32   0    3G  0 disk 
sr0                11:0    1 1024M  0 rom  

[arthur@node1 ~]$ sudo pvcreate /dev/sdb

[arthur@node1 ~]$ sudo pvcreate /dev/sdc

[arthur@node1 ~]$ sudo pvs
  PV         VG       Fmt  Attr PSize  PFree
  /dev/sda3  rl_rocky lvm2 a--  62.41g    0 
  /dev/sdb            lvm2 ---   3.00g 3.00g
  /dev/sdc            lvm2 ---   3.00g 3.00g
[arthur@node1 ~]$ sudo vgcreate partsgroup /dev/sdb

[arthur@node1 ~]$ sudo vgextend partsgroup /dev/sdc

[arthur@node1 ~]$ sudo vgs
  VG         #PV #LV #SN Attr   VSize  VFree
  partsgroup   2   0   0 wz--n-  5.99g 5.99g
  rl_rocky     1   3   0 wz--n- 62.41g    0 
[arthur@node1 ~]$ sudo lvcreate -L 1G partsgroup -n casse_pas_les_couilles
[arthur@node1 ~]$ sudo lvcreate -L 1G partsgroup -n casse_pas_les_couilles-2
[arthur@node1 ~]$ sudo lvcreate -L 1G partsgroup -n casse_pas_les_couilles-3
[arthur@node1 ~]$ sudo lvs
  LV           VG         Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  ca_data_frer partsgroup -wi-a-----   1.00g                                                    
  ma_data_frer partsgroup -wi-a-----   1.00g                                                    
  ta_data_frer partsgroup -wi-a-----   1.00g                                                    
  home         rl_rocky   -wi-ao----  19.48g                                                    
  root         rl_rocky   -wi-ao---- <39.91g                                                    
  swap         rl_rocky   -wi-ao----  <3.02g                                                    
[arthur@node1 ~]$ sudo mkfs -t ext4 /dev/partsgroup/casse_pas_les_couilles-3
[arthur@node1 ~]$ sudo mkfs -t ext4 /dev/partsgroup/casse_pas_les_couilles-2
[arthur@node1 ~]$ sudo mkfs -t ext4 /dev/partsgroup/casse_pas_les_couilles
[arthur@node1 ~]$ sudo mkdir /mnt/part1
[arthur@node1 ~]$ sudo mkdir /mnt/part2
[arthur@node1 ~]$ sudo mkdir /mnt/part3
[arthur@node1 ~]$ sudo mount /dev/partsgroup/casse_pas_les_couilles /mnt/part1
[arthur@node1 ~]$ sudo mount /dev/partsgroup/casse_pas_les_couilles-2 /mnt/part2
[arthur@node1 ~]$ sudo mount /dev/partsgroup/casse_pas_les_couilles-3 /mnt/part3
[arthur@node1 ~]$ df -h
Filesystem                           Size  Used Avail Use% Mounted on
[...]
/dev/mapper/partsgroup-casse_pas_les_couilles  974M   24K  907M   1% /mnt/part1
/dev/mapper/partsgroup-casse_pas_les_couilles-2  974M   24K  907M   1% /mnt/part2
/dev/mapper/partsgroup-casse_pas_les_couilles-3  974M   24K  907M   1% /mnt/part3
```

🌞 **Grâce au fichier `/etc/fstab`**
```
[arthur@node1 ~]$ cat /etc/fstab
[...]
/dev/partsgroup/ma_data_frer /mnt/part1 ext4 defaults 0 0
/dev/partsgroup/ta_data_frer /mnt/part2 ext4 defaults 0 0
/dev/partsgroup/ca_data_frer /mnt/part3 ext4 defaults 0 0
```

## III. Gestion de services

## 1. Interaction avec un service existant


🌞 **Assurez-vous que...**

```
[arthur@node1 ~]$ systemctl status firewalld
● firewalld.service - firewalld - dynamic firewall daemon
     Loaded: loaded (/usr/lib/systemd/system/firewalld.service; enabled; vendor preset: enabled)
     Active: active (running) since Mon 2022-11-14 17:33:04 CET; 45min ago
       Docs: man:firewalld(1)
   Main PID: 720 (firewalld)
      Tasks: 3 (limit: 15875)
     Memory: 47.6M
        CPU: 851ms
     CGroup: /system.slice/firewalld.service
             └─720 /usr/bin/python3 -s /usr/sbin/firewalld --nofork --nopid
```

## 2. Création de service

### A. Unité simpliste

🌞 **Créer un fichier qui définit une unité de service** 

```
[arthur@node1 ~]$ cat /etc/systemd/system/web.service 
[Unit]
Description=Very simple web service

[Service]
ExecStart=/usr/bin/python3 -m http.server 8888

[Install]
WantedBy=multi-user.target
```

```
[arthur@node1 ~]$ systemctl status web
○ web.service - Very simple web service
     Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: disabled)
     Active: inactive (dead)
[arthur@node1 ~]$ sudo systemctl start web
[arthur@node1 ~]$ systemctl status web
● web.service - Very simple web service
     Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: disabled)
     Active: active (running) since Mon 2022-11-14 18:30:00 CET; 5s ago
   Main PID: 1907 (python3)
      Tasks: 1 (limit: 15875)
     Memory: 9.1M
        CPU: 39ms
     CGroup: /system.slice/web.service
             └─1907 /usr/bin/python3 -m http.server 8888

Nov 14 18:30:00 node1.tp2.b2 systemd[1]: Started Very simple web service.
[arthur@node1 ~]$ sudo systemctl enable web
Created symlink /etc/systemd/system/multi-user.target.wants/web.service → /etc/systemd/system/web.service.
```

🌞 **Une fois le service démarré, assurez-vous que pouvez accéder au serveur web**

```
[arthur@node2 ~]$ curl node1.tp1.b2:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="afs/">afs/</a></li>
<li><a href="bin/">bin@</a></li>
<li><a href="boot/">boot/</a></li>
<li><a href="dev/">dev/</a></li>
<li><a href="etc/">etc/</a></li>
<li><a href="home/">home/</a></li>
<li><a href="lib/">lib@</a></li>
<li><a href="lib64/">lib64@</a></li>
<li><a href="media/">media/</a></li>
<li><a href="mnt/">mnt/</a></li>
<li><a href="opt/">opt/</a></li>
<li><a href="proc/">proc/</a></li>
<li><a href="root/">root/</a></li>
<li><a href="run/">run/</a></li>
<li><a href="sbin/">sbin@</a></li>
<li><a href="srv/">srv/</a></li>
<li><a href="sys/">sys/</a></li>
<li><a href="tmp/">tmp/</a></li>
<li><a href="usr/">usr/</a></li>
<li><a href="var/">var/</a></li>
</ul>
<hr>
</body>
</html>
```

### B. Modification de l'unité

🌞 **Préparez l'environnement pour exécuter le mini serveur web Python**

```
[arthur@node1 ~]$ sudo ls -al /var/www/meow
total 0
drwxr--r--. 2 web  web  29 Nov 14 18:49 .
drwxr-xr-x. 3 root root 18 Nov 14 18:39 ..
-rwxr--r--. 1 web  web   0 Nov 14 18:49 veryprivatefile
```

🌞 **Modifiez l'unité de service `web.service` créée précédemment en ajoutant les clauses**

```
[arthur@node1 ~]$ cat /etc/systemd/system/web.service 
[Unit]
Description=Very simple web service

[Service]
ExecStart=/usr/bin/python3 -m http.server 8888
User=web
WorkingDirectory=/var/www/meow/


[Install]
WantedBy=multi-user.target
```
🌞 **Vérifiez le bon fonctionnement avec une commande `curl`**

```
[arthur@node2 ~]$ curl node1.tp1.b2:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="veryprivatefile">veryprivatefile</a></li>
</ul>
<hr>
</body>
</html>
```